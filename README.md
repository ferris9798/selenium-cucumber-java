# selenium-cucumber-java

Automate UI testing using Selenium and Cucumber with Page Object Model

## Prerequisite

Java JDK 1.8, Maven

## Getting started

Copy the repo into your local machine.

## Configure

Navigate to config.properties file to change the URL

## Test result

* See the result inside `target/report/` folders that automatically generated after finished the test execution
* Open html file in your browser
