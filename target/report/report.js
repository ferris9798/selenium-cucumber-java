$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("search.feature");
formatter.feature({
  "line": 1,
  "name": "Search with keyword customer solution",
  "description": "",
  "id": "search-with-keyword-customer-solution",
  "keyword": "Feature"
});
formatter.before({
  "duration": 8935548902,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "User want to search customer solution",
  "description": "",
  "id": "search-with-keyword-customer-solution;user-want-to-search-customer-solution",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User open verint web",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User type the keyword \"customer solution\"",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User want to check the result \"customer solution\"",
  "keyword": "Then "
});
formatter.match({
  "location": "searchingStep.userOpenVerint()"
});
formatter.result({
  "duration": 145607308,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "customer solution",
      "offset": 23
    }
  ],
  "location": "searchingStep.userTypeTheKeyword(String)"
});
formatter.result({
  "duration": 2587061994,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "customer solution",
      "offset": 31
    }
  ],
  "location": "searchingStep.userWantToCheckTheResult(String)"
});
formatter.result({
  "duration": 175377,
  "status": "passed"
});
formatter.after({
  "duration": 101980200,
  "status": "passed"
});
});