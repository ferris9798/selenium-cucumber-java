package stepsDefinition;

import cucumber.api.PendingException;
import pages.HomePage;
import pages.ResultPage;
import setups.PropertiesReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SearchSteps {
	private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;

    public SearchSteps() throws Exception {
        PropertiesReader propertiesReader = new PropertiesReader();
        this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
    }

	@Given("^User open verint web$")
    public void userOpenVerint() {
		HomePage home = new HomePage(driver, wait);
		home.homePageIsDisplayed();
		
    }

	@Then("^User type the keyword \"(.*)\"$")
    public void userTypeTheKeyword(String keyword) throws Throwable {
		HomePage home = new HomePage(driver, wait);
		home.clickSearchIcon();
		home.inputKeyword(keyword);
		home.pressEnter();
    }
	
	@Then("^User want to check the result \"(.*)\"$")
    public void userWantToCheckTheResult(String keyword) throws Throwable {
        ResultPage result = new ResultPage(driver, wait);
		result.searchResultPageIsDisplayed();
		result.checkResultTitle(keyword);
    }
}
