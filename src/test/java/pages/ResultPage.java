package pages;

import org.openqa.selenium.Keys;
import setups.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;


public class ResultPage extends BaseClass {
	public ResultPage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "main-content")
    private WebElement mainContent;
    
    @FindBy(className = "sj-results__result__title")
    private WebElement resultTitle;

    public boolean searchResultPageIsDisplayed() {
        WaitUntilElementVisible(mainContent);
        mainContent.isDisplayed();
        assertTrue(mainContent.getText().toString().contains("Search Results"));
        return true;
    }
    
    public boolean checkResultTitle(String keyword) {
        WaitUntilElementVisible(resultTitle);
        resultTitle.isDisplayed();
        
        List<String> keywordSplit = Arrays.asList(keyword.toLowerCase().split(" "));

        assertTrue(keywordSplit.stream().anyMatch(resultTitle.getText().toString().toLowerCase()::contains));
        return true;
    }
}
