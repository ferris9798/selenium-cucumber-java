package pages;

import org.openqa.selenium.Keys;
import setups.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class HomePage extends BaseClass {
	public HomePage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "nav-toggle")
    private WebElement searchIcon;
    
    @FindBy(id = "downshift-0-input")
    private WebElement searchField;

    @FindBy(className = "sj-input__button css-1k4nm74")
    private WebElement searchBtn;
    

    public boolean homePageIsDisplayed() {
        WaitUntilElementVisible(searchIcon);
        return searchIcon.isDisplayed();
    }
    
    public void clickSearchIcon() throws InterruptedException {
    	WaitUntilElementVisible(searchIcon);
    	searchIcon.click();
    }
    
    public void inputKeyword(String keyword) throws InterruptedException {
        searchField.sendKeys(keyword);
    }

    public void clickSearchBtn() throws InterruptedException {
        searchBtn.click();
    }

    public void pressEnter(){
        searchField.sendKeys(Keys.RETURN);
    }
}
